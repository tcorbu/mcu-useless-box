#include <Energia.h>
#include <Servo.h>

// PINS
#define BUTTON_LIFT_SERVO_PIN P2_1
#define ARM_SERVO_PIN P2_2
#define LEFT_BUTTON_PIN P2_4
#define RIGHT_BUTTON_PIN P2_5

Servo armServo;
Servo liftServo;

bool maintainMode = false;

void buttonIntrerupt(){
  wakeup();
}

void pushIntrerupt(){
  wakeup();
}

void raiseButton(){
  liftServo.attach(P1_6, 500, 2400);
  liftServo.write(75);
  delay(600);
  liftServo.detach();
}

void lowerButton(){
  liftServo.attach(P1_6, 500, 2400);
  liftServo.write(0);
  delay(600);
  liftServo.detach();
}

void lowerArm(){
  armServo.attach(P2_6, 500, 2400);
  armServo.write(60);
  delay(600);
  armServo.detach();
}

void retractArm(){
  armServo.attach(P2_6, 500, 2400);
  armServo.write(85);
  delay(600);
  armServo.detach();
}

void extendArm(){
  armServo.attach(P2_6, 500, 2400);
  armServo.write(150);
  delay(600);
  armServo.detach();
}

void moveArmFrom(uint8_t fromGrads,uint8_t toGrads, uint8_t sleepEachStep){
  armServo.attach(P2_6, 500, 2400);
  for (uint8_t i = fromGrads; i<toGrads; i++){
    armServo.write(i);
    delay(sleepEachStep);
  }
  armServo.detach();
}

void moveArm(uint8_t grads, uint16_t sleepFor){
  armServo.attach(P2_6, 500, 2400);
  armServo.write(grads);
  delay(sleepFor);
  armServo.detach();
}

void setup(){
  //Serial.begin(9600);
  pinMode(P2_3, INPUT_PULLDOWN);
  attachInterrupt(P2_4, buttonIntrerupt, RISING);
  pinMode(P2_4, INPUT_PULLDOWN);
  attachInterrupt(P2_4, buttonIntrerupt, RISING);
  pinMode(P2_5, INPUT_PULLDOWN);
  attachInterrupt(P2_5, pushIntrerupt, RISING);
  pinMode(P1_0, INPUT);


  lowerButton();
  lowerArm();
  sleep(10000);
  retractArm();
  raiseButton();

}

void loop(){
  delay(500);
  uint8_t routine = analogRead(P1_0)%5;
  switch(routine){
    case 0:
      extendArm();
      retractArm();
      break;
    case 1:
      lowerButton();
      delay(1000);
      raiseButton();
      extendArm();
      retractArm();
      break;
    case 2:
      moveArm(120, 600);
      moveArmFrom(120, 150, 50);
      retractArm();
      break;
    case 3:
      moveArm(120, 600);
      retractArm();
      lowerButton();
      sleep(10000);
      raiseButton();
      extendArm();
      retractArm();
      break;
    default:
      lowerButton();
      lowerArm();
      extendArm();
      retractArm();
      raiseButton();
      extendArm();
      retractArm();
  }
  suspend();
}
