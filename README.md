# Useless Box

The useless box with two axes: one for the arm and another for a switch lift.

This device makes use of MSP430G2335 chip witch is a low end and low power microcontroller from Texas Instruments.

The firmware was compiled with the help of [PlatformIO][1] and it was uploaded with the [LaunchPad MPS430G2][2] development board using an Arduino alike abstraction: [Energia][3].

## Mechanical schematic

This is how I've picture it :
![ Mechanical schematic doodle][mechanical-schematic-doodle]

And this is more or less the end measurements
![ Mechanical schematic measurements][mechanical-schematic]

I choose wood as primary material for this construction

## Electric schematic

I made this Fritzing schematic that shows the components that this projects uses:

BOM:

    1 x Double AA Battery holder
    3 x 47k Resistors
    1 x Push Button - for reset
    2 x Micro Servos 9g  
    1 x Mechanical Encoder
    1 x MSP430G2553
    1 x Prefboard

Breadboard sketch:

![ Electric schematic breadboard][electric-schematic-breadboard]

Schematic:
![ Mechanical schematic measurements][electric-schematic]

The whole fritzing project lies on this repo, under the assets folder : [./assets/fritzing/sketch.fzz][4]

[1]: http://platformio.org/
[2]: http://www.ti.com/tool/msp-exp430g2
[3]: http://energia.nu/
[4]: ./assets/fritzing/sketch.fzz
[mechanical-schematic]: ./assets/useless_box_mechanical_sketch.svg "Mechanical schematic"
[mechanical-schematic-doodle]: ./assets/useless_box_mechanical_sketch.png "Mechanical schematic doodle"
[electric-schematic-breadboard]: ./assets/fritzing/sketch_bb.png "Electric schematic breadboard"
[electric-schematic]: ./assets/fritzing/sketch_schem.png "Electric schematic"
